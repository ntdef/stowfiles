-- plugins.lua
--
-- Plugin configuration.
--

return require('packer').startup(function()
	-- TODO Packages to try
	-- use '9mm/vim-closer'
	-- use 'Olical/aniseed'
	-- use 'direnv/direnv.vim'
	-- use 'reedes/vim-pencil'
	-- use 'tpope/vim-dadbod'
	-- use 'vim-pandoc/vim-pandoc'
	-- use 'mhinz/vim-grepper'
	-- use 'vim-pandoc/vim-pandoc-syntax'
	-- use 'tjdevries/astronauta.nvim'
	-- use 'romainl/vim-qf'
	-- use 'norcalli/snippets.nvim'
	-- use 'glepnir/lspsaga.nvim'

	local popup = 'nvim-lua/popup.nvim'
	local plenary = 'nvim-lua/plenary.nvim'

	use 'axelf4/vim-strip-trailing-whitespace'
	use 'mhinz/vim-signify'
	use 'elzr/vim-json'
	use 'kevinoid/vim-jsonc' -- a fork of vim-json with support for comments
	use 'plasticboy/vim-markdown'
	use 'wbthomason/packer.nvim'
	use 'nvim-lua/completion-nvim'
	use 'neovim/nvim-lspconfig'
	use 'pearofducks/ansible-vim'
	-- treesitter
	use 'nvim-treesitter/nvim-treesitter'
	use 'nvim-treesitter/nvim-treesitter-textobjects'
	use 'nvim-treesitter/playground'

	use 'rhysd/clever-f.vim'
	use 'jiangmiao/auto-pairs'
	use 'machakann/vim-sandwich'
	use 'morhetz/gruvbox'

	use {'nvim-telescope/telescope.nvim', requires = { plenary, popup } }
	use 'svermeulen/vim-yoink'
	use 'reedes/vim-pencil'

	use 'tpope/vim-abolish'
	use 'tpope/vim-apathy'
	use 'tpope/vim-commentary'
	use 'tpope/vim-dispatch'
	use 'tpope/vim-endwise'
	use 'tpope/vim-fugitive'
	use 'tpope/vim-rhubarb'
	use 'tpope/vim-sensible'
	use 'tpope/vim-sleuth'
	use 'tpope/vim-unimpaired'
	use 'tpope/vim-projectionist'
	use 'tpope/vim-eunuch'
end)


