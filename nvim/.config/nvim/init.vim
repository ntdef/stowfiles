" TODO Margin for GUI Vim: https://stackoverflow.com/questions/7941427/macvim-how-do-i-set-a-left-gutter-margin-for-my-buffers
" TODO Read through this VIM doc: https://github.com/mhinz/vim-galore#autocmds
" TODO Great dotfiles to look at: https://github.com/mhinz/dotfiles
" TODO switch from init.vim to init.lua: https://icyphox.sh/blog/nvim-lua/
" TODO build plugin to search github with regex: https://grep.app
" TODO grab aliases from oh-my-zsh https://github.com/ohmyzsh/ohmyzsh/wiki/Cheatsheet
" TODO https://www.reddit.com/r/zsh/
" TODO learn go with tests https://github.com/quii/learn-go-with-tests/tree/master
" TODO learn vimscript the hard way https://learnvimscriptthehardway.stevelosh.com/
" TODO https://github.com/danielfm/spotify.el
" TODO Read JoelOnSoftware's reading lists
"
lua require('plugins')

let $NVIM_TUI_ENABLE_TRUE_COLOR=1
let g:vimsyn_embed='l'

set rtp+=/usr/local/opt/fzf
set termguicolors
set hidden
set completeopt=menuone,noinsert,noselect 
set shortmess+=c
set tabstop=4
set shiftwidth=4
set expandtab
set clipboard=unnamedplus               " Copy paste between vim and everything else
set updatetime=100
set lazyredraw
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()

" See https://gist.github.com/romainl/379904f91fa40533175dfaec4c833f2f
" on why you have to override highlights this way
fun! GruvboxHighlights() abort
    set foldcolumn=1
    hi link Function GruvboxBlue
    hi FoldColumn ctermbg=0
    hi SignColumn ctermbg=0
endfun

aug MyColors
    au!
    au ColorScheme gruvbox call GruvboxHighlights()
aug END

set bg=dark
colo gruvbox

let g:fzf_layout = {'window': { 'width': 0.7, 'height': 0.5, 'yoffset': 0.2, 'border': 'rounded' } }
let g:fzf_history_dir = '$HOME/.local/share/fzf-history'

augroup highlight_yank
    autocmd!
    autocmd TextYankPost * silent! lua require'vim.highlight'.on_yank{higroup = 'Substitute', timeout = 200}
augroup END

let mapleader=" "


nnoremap Q @@
nnoremap <leader>cd :cd %:p:h<CR>:pwd<CR>
nnoremap <leader>ec :e $MYVIMRC<cr>
nnoremap <leader>ep :e $HOME/.config/nvim/lua/plugins.lua<cr>
nnoremap <leader><leader>s :so %<cr>
nnoremap <leader>gg :G<cr>
nnoremap <leader>og :!open.sh https://github.com/<c-r><c-f><cr><cr>
nnoremap <leader>q :qa!<cr>
nnoremap <leader>s :w<cr>

" <C-w> rebind
nnoremap <leader>w <C-w>

" Telescope
nnoremap <leader>bb <cmd>Telescope buffers<cr>
nnoremap <leader>gf <cmd>Telescope git_files<cr>
nnoremap <leader>pf <cmd>Telescope git_files<cr>

nnoremap <silent> <TAB> <Cmd>bnext<cr>
nnoremap <silent> <S-TAB> <Cmd>bprevious<cr>

" Better tabbing
vnoremap < <gv
vnoremap > >gv

nnoremap <C-j> <Cmd>cnext<cr>
nnoremap <C-k> <Cmd>cprev<cr>

" Make esc leave terminal mode
tnoremap <leader><Esc> <C-\><C-n>
tnoremap <Esc><Esc> <C-\><C-n>

" remap numbers to symbols in normal mode
"
" set langmap=1!,2@,3#,4$,5%,6^,7&,8*,9(,0),_-,!1,@2,#3,$4,%5,^6,&7,*8,(9,)0,-_

augroup vimrc
    autocmd!
    autocmd BufWritePost plugins.lua,init.vim PackerCompile
    " TODO Move this to a filetype plugin for init.vim
    autocmd BufReadPost $MYVIMRC set suffixesadd+=.lua | let &l:path.=",".stdpath("config")."/**"
    autocmd BufWritePost $MYVIMRC so %
augroup END


function! Scratch()
    split
    noswapfile hide enew
    setlocal buftype=nofile bufhidden=hide buflisted
    file scratch
endfunction



lua <<EOF
require'nvim-treesitter.configs'.setup {
  highlight = { enable = true },
  indent = { enable = true },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = "gnn",
      node_incremental = "grn",
      scope_incremental = "grc",
      node_decremental = "grm",
    }
  }
}
EOF


lua <<EOF
require "nvim-treesitter.configs".setup {
  playground = {
    enable = true,
    disable = {},
    updatetime = 25, -- Debounced time for highlighting nodes in the playground from source code
    persist_queries = false -- Whether the query persists across vim sessions
  }
}
EOF

lua << EOF
local lspconfig = require("lspconfig")
lspconfig.clangd.setup{}
local custom_lsp_attach = function(client)
    -- See `:help nvim_buf_set_keymap()` for more information
    vim.api.nvim_buf_set_keymap(0, 'n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', {noremap = true})
    vim.api.nvim_buf_set_keymap(0, 'n', '<c-]>', '<cmd>lua vim.lsp.buf.definition()<CR>', {noremap = true})
    -- ... and other keymappings for LSP

    -- Use LSP as the handler for omnifunc.
    --    See `:help omnifunc` and `:help ins-completion` for more information.
    vim.api.nvim_buf_set_option(0, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

    -- For plugins with an `on_attach` callback, call them here. For example:
    -- require('completion').on_attach()
end

-- An example of configuring for `sumneko_lua`,
--  a language server for Lua.

-- set the path to the sumneko installation
local system_name = "macOS" -- (Linux, macOS, or Windows)
local sumneko_root_path = vim.fn.expand('$HOME') .. '/.local/src/lua-language-server'
local sumneko_binary = sumneko_root_path.."/bin/"..system_name.."/lua-language-server"

lspconfig.sumneko_lua.setup({
    cmd = {sumneko_binary, "-E", sumneko_root_path .. "/main.lua"};
    -- an example of settings for an lsp server.
    --    for more options, see nvim-lspconfig
    filetypes = { 'lua' };
    settings = {
      Lua = {
        runtime = {
          -- tell the language server which version of lua you're using (most likely luajit in the case of neovim)
          version = 'LuaJIT',
          -- setup your lua path
          path = vim.split(package.path, ';'),
        },
        diagnostics = {
          enable = true,
          -- get the language server to recognize the `vim` global
          globals = {'vim'},
        },
        workspace = {
          -- make the server aware of neovim runtime files
          library = {
            [vim.fn.expand('$VIMRUNTIME/lua')] = true,
            [vim.fn.expand('$VIMRUNTIME/lua/vim/lsp')] = true,
          },
        },
      }
    },
    on_attach = require'completion'.on_attach
})

vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
    vim.lsp.diagnostic.on_publish_diagnostics,
    {
      -- Enable underline, use default values
      underline = true,
      -- Enable virtual text, override spacing to 4
      virtual_text = true,
      signs = {
        enable = true,
        priority = 20
      },
      -- Disable a feature
      update_in_insert = false
    }
  )
EOF

