;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Troy de Freitas"
      user-mail-address "troyd@spotify.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-gruvbox)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


(setq ns-right-alternate-modifier 'left)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; Treat journal files as markdown
(add-to-list 'auto-mode-alist '("/\\log/.*\.txt$" . org-journal-mode))

(defcustom ntdef/journal-dir "/Users/troyd/.local/share/log/"
  "Location of daily journal files.")

(setq org-journal-dir ntdef/journal-dir
      org-journal-file-format "%Y-%m-%d.txt"
      org-journal-enable-agenda-integration t)

(defun ntdef/journal-visit-today ()
  "Visit today's journal entry."
  (interactive)
  (let* ((today (format-time-string "%Y-%m-%d"))
          (fname (concat today ".txt"))
          (fpath (concat (file-name-as-directory ntdef/journal-dir) fname)))
    (find-file fpath)))

(defun ntdef/journal-visit-today-focus-frame ()
  (x-focus-frame nil)
  (ntdef/journal-visit-today))



(after! python
  (setq python-shell-interpreter "python3"))

;; FORGE
(after! forge
  (add-to-list 'forge-alist
               '("ghe.spotify.net" "ghe.spotify.net/api/v3" "ghe.spotify.net" forge-github-repository)))

;; needed for forge to pick up token
(setq auth-sources '("~/.authinfo.gpg" "~/.authinfo" "~/.netrc"))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(git-link-remote-alist
   '(("git.sr.ht" git-link-sourcehut)
     ("ghe.spotify.net" git-link-github)
     ("github" git-link-github)
     ("bitbucket" git-link-bitbucket)
     ("gitorious" git-link-gitorious)
     ("gitlab" git-link-gitlab)
     ("visualstudio\\|azure" git-link-azure)))
 '(package-selected-packages '(ox-slack git-link)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; open-buffer-with
;; Open buffer with contents
;; (defun ntdef/open-buffer-with (body)
;;   (interactive))
(require 'evil-easymotion)
(require 'evil-escape)
(require 'evil-snipe)

(use-package! tree-sitter
  :config
  (require 'tree-sitter-langs)
  (global-tree-sitter-mode)
  (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))
