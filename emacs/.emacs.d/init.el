;; setup straight.el
;;
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)

(setq straight-use-package-by-default t)

;; custom set file
(setq custom-file (concat user-emacs-directory "custom.el"))
(load custom-file 'noerror)

;; Show time
(display-time-mode 1)

;; Remove scroll bar
(scroll-bar-mode -1)
(tool-bar-mode -1)

;; Turn on winner mode
(winner-mode 1)

;; Sentences end with a single space
(setq sentence-end-double-space nil)

;; Use y or n, neve yes or no
(defalias 'yes-or-no-p 'y-or-n-p)

;; delete excess backup versions silently
(setq delete-old-versions -1)

;; use version control
(setq version-control t)

;; don't ask questions when opening sym linked files
(setq vc-follow-symlinks t)

;; don't be annoying
(setq ring-bell-function 'ignore)

;; default wrap length is 80 chars
(setq default-fill-column 80)

;; set directroy for backup files
(setq backup-directory-alist `(("." . "~/.emacs.d/backups")))

;; use utf-8 by default
(setq coding-system-for-read 'utf-8
      coding-system-for-write 'utf-8)

;; automatically delete trailing whitespace
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; Treat journal files as markdown
(add-to-list 'auto-mode-alist '("/\\log/.*\.txt$" . org-mode))

;; Turn on abbrev mode everywhere
(setq-default abbrev-mode t)

;; EL-PATCH
(use-package el-patch)

;; BETTER-DEFAULTS
(use-package better-defaults)

;; THEMES
(use-package gruvbox-theme
  :init (load-theme 'gruvbox))

(use-package modus-operandi-theme
  :config
  ;; LOAD-THEME
  ;; (load-theme 'modus-operandi t)
  )

(use-package modus-vivendi-theme)

;; VTERM
(use-package vterm)

;; MULTIPLE-CURSORS
(use-package multiple-cursors)

;; IVY
(use-package ivy
  :diminish ""
  :init (ivy-mode 1)
  :config
  (setq ivy-use-virtual-buffers 1
	ivy-height 10
	ivy-use-selectable-prompt 1)
  :bind
  (("C-c v" . 'ivy-push-view)
   ("C-c V" . 'ivy-pop-view)
   ("C-x b" . 'ivy-switch-buffer)
   ("<C-tab>" . 'ivy-switch-buffer)
   ("C-c C-r" . 'ivy-resume)
   ("C-c v" . 'ivy-push-view)
   ("C-c V" . 'ivy-pop-view)))

;; MAGIT
(use-package magit)

;; RESTCLIENT
(use-package restclient
  ;; :hook (restclient-response-mode . 'read-only-mode)
  :config
  (defun ntdef/restclient-advice (orig-fun &rest args)
    (let ((inhibit-read-only t))
      (apply orig-fun args)))
  (advice-add 'restclient-http-handle-response :around
	      'ntdef/restclient-advice))

(use-package ob-restclient)

;; FLYCHECK
(use-package flycheck)

(defun ntdef/indent-buffer ()
  (interactive)
  (save-excursion
    (indent-region (point-min) (point-max) nil)))

(defun end-of-line-and-indented-new-line ()
  (interactive)
  (end-of-line)
  (newline-and-indent))

(defun append-to-line ()
  (interactive)
  (end-of-line)
  (modalka-off))

(defun ntdef/mark-word-fwd (N)
  (interactive "p")
  (if (and
       (not (eq last-command this-command))
       (not (eq last-command 'ntdef/mark-word-bwd)))
      (set-mark (point)))
  (forward-word N))

(defun ntdef/mark-word-bwd (N)
  (interactive "p")
  (if (and
       (not (eq last-command this-command))
       (not (eq last-command 'ntdef/mark-word-fwd)))
      (set-mark (point)))
  (backward-word N))


;; ALIASESS
(defalias 'indent-buffer 'ntdef/indent-buffer)
(defalias 'mark-word-fwd 'ntdef/mark-word-fwd)
(defalias 'mark-word-bwd 'ntdef/mark-word-bwd)


;; EXEC-PATH-FROM-SHELL
(use-package exec-path-from-shell
  :init
  (progn
    (exec-path-from-shell-initialize)))

(use-package smartparens
  :init (smartparens-mode))

;; EXPAND-REGION
(use-package expand-region)

(use-package modalka
  ;; require expand-region
  :init
  (modalka-global-mode)

  :config
  (setq-default cursor-type '(bar . 1))
  (setq modalka-cursor-type 'box)

  (defun modalka-off ()
    (interactive)
    (modalka-mode -1))

  (defun modalka-on ()
    (interactive)
    (modalka-mode 1))

  (defun ntdef/change-region ()
    (interactive)
    (kill-region (region-beginning) (region-end))
    (modalka-off))

  ;; https://stackoverflow.com/questions/2173324
  (defun ntdef/vi-open-line-above ()
    "Insert a newline above the current line and put point at beginning."
    (interactive)
    (unless (bolp)
       (beginning-of-line))
    (newline)
    (forward-line -1)
    (indent-according-to-mode)
    (modalka-off))

  (defun ntdef/vi-open-line-below ()
    "Insetrt a newline below the current line and put point at beginning."
    (interactive)
    (unless (eolp)
      (end-of-line))
    (newline-and-indent)
    (modalka-off))

  (defun ntdef/mark-line (&optional arg allow-extend)
    "Set mark ARG lines away from point.
The place mark goes is the same place \\[forward-line] would
move to with the same argument.
Interactively, if this command is repeated
or (in Transient Mark mode) if the mark is active,
it marks the next ARG lines after the ones already marked."
    ;; taken from emacs's internal mark-word
    (interactive "P\np")
    (cond ((and allow-extend
		(or (and (eq last-command this-command) (mark t))
		    (region-active-p)))
	   (setq arg (if arg (prefix-numeric-value arg)
		       (if (< (mark) (point)) -1 1)))
	   (set-mark
	    (save-excursion
	      (goto-char (mark))
	      (forward-line arg)
	      (point))))
	  (t
	   (push-mark
	    (save-excursion
	      (forward-line (prefix-numeric-value arg))
	      (point))
	    nil t))))

  (defalias 'mark-line 'ntdef/mark-line)

  :bind
  (("C-^" . 'modalka-on)))


(use-package modalka
  :config
  (defun ntdef/forward-same-syntax ()
    (interactive)
    (set-mark (point))
    (forward-same-syntax))

  (defun ntdef/backward-same-syntax (count)
    (interactive "p")
    (set-mark (point))
    (forward-same-syntax (- count)))

  (defun ntdef/kakoune-d (count)
    (interactive "p")
    (if (use-region-p)
	(kill-region (region-beginning) (region-end)) (delete-char count t)))

  (defun ntdef/change-and-exit (count)
    (interactive "p")
    (ntdef/kakoune-d count)
    (modalka-off))

  (defun ntdef/set-mark-here ()
    (interactive)
    (set-mark (point)))

  (defun ntdef/deactivate-mark ()
    (interactive)
    (unless rectangle-mark-mode (deactivate-mark)))

  (defun ntdef/forward-char-and-deactivate (count)
    (interactive "p")
    (forward-char count)
    (ntdef/deactivate-mark))

  (defun ntdef/backward-char-and-deactivate (count)
    (interactive "p")
    (backward-char count)
    (ntdef/deactivate-mark))

  (defun ntdef/next-line-and-deactivate (count)
    (interactive "p")
    (next-line count)
    (ntdef/deactivate-mark))

  (defun ntdef/previous-line-and-deactivate (count)
    (interactive "p")
    (previous-line count)
    (ntdef/deactivate-mark))

  (defun ntdef/forward-char-then-exit ()
    (interactive)
    (forward-char)
    (modalka-off))

  (defun ntdef/forward-word-and-mark ()
    (interactive)
    (ntdef/set-mark-here)
    (forward-word))

  (defun ntdef/backward-word-and-mark ()
    (interactive)
    (ntdef/set-mark-here)
    (backward-word))

  (defun ntdef/beginning-of-line-and-mark ()
    (interactive)
    (ntdef/set-mark-here)
    (beginning-of-line))

  (defun ntdef/end-of-line-and-mark ()
    (interactive)
    (ntdef/set-mark-here)
    (end-of-line))

  (defun ntdef/beginning-of-paragraph-and-mark ()
    (interactive)
    (ntdef/set-mark-here)
    (backward-paragraph))

  (defun ntdef/end-of-paragraph-and-mark ()
    (interactive)
    (ntdef/set-mark-here)
    (forward-paragraph))

  (defun ntdef/append ()
    (interactive)
    (forward-char)
    (modalka-off))

  (defun ntdef/edit ()
    (interactive)
    (when (use-region-p)
      (kill-region (region-beginning) (region-end)))
    (modalka-off))

  (defun ntdef/append-to-end-of-line ()
    (interactive)
    (end-of-line)
    (modalka-off))

  (defun kakoune-x (count)
  "Select COUNT lines from the current line.
Note that kakoune's x doesn't behave exactly like this,
but I like this behavior better."
  (interactive "p")
  (beginning-of-line)
  (set-mark (point))
  (forward-line count))

  :bind
  (:map modalka-mode-map
	;; memnonic
	("a" . #'ntdef/append)
	("A" . #'ntdef/append-to-end-of-line)
	("n" . #'ntdef/next-line-and-deactivate)
	("N" . #'next-line)
	("p" . #'ntdef/previous-line-and-deactivate)
	("P" . #'previous-line)
	("f" . #'ntdef/forward-char-and-deactivate)
	("F" . #'forward-char)
	("b" . #'ntdef/backward-char-and-deactivate)
	("B" . #'backward-char)
	("e" . #'ntdef/edit)
	;;	("E" . #'ntdef/edit-at-line-start)
	;;        ("E" . ???)
	("z" . #'undo)
	("x" . #'kill-region) ;; FIXME
	("c" . #'copy-region-as-kill)
	("v" . #'yank)
	("w" . #'ntdef/forward-word-and-mark)
	("W" . #'forward-word)
	;;	("d" . #'ntdef/forward-word-end-and-mark)
	("d" . #'ntdef/backward-word-and-mark)
	("D" . #'backward-word)
	("l" . #'kakoune-x)
	("L" . #'next-line) ;; FIXME
	("h" . #'mark-paragraph)
	("r" . #'repeat)
	("i" . #'isearch-forward)
	;;("s" . #'ntdef/select-on-regex)
	;;("S" . #'ntdef/split-on-regex)
	("o" . #'ntdef/vi-open-line-below)
	("O" . #'ntdef/vi-open-line-above)

	("t" . #'push-mark-command)
	("j" . #'exchange-point-and-mark) ;; jump
	(";" . #'execute-extended-command)
	))

;; ;; MODALKA COLEMAK
;; (use-package modalka
;;   :bind
;;   (("<escape>" . 'modalka-on)
;;    ("C-x C-u" . 'upcase-dwim)
;;    :map modalka-mode-map
;;    ;; TODO add extend region (uppercase) commands
;; 	("u" . 'ntdef/previous-line-and-deactivate)
;; 	("e" . 'ntdef/next-line-and-deactivate)
;; 	("n" . 'backward-char)
;; 	("i" . 'forward-char)
;; 	("l" . 'ntdef/backward-word-and-mark)
;; 	("y" . 'ntdef/forward-word-and-mark)
;; 	("t" . 'modalka-off)
;; 	("o" . 'ntdef/end-of-line-and-mark)
;; 	("O" . 'end-of-line)
;; 	("h" . 'ntdef/beginning-of-line-and-mark)
;; 	("H" . 'beginning-of-line)
;; 	("m" . 'ntdef/beginning-of-paragraph-and-mark)
;; 	("," . 'ntdef/end-of-paragraph-and-mark)
;; 	("j" . 'undo)
;; 	("a" . 'execute-extended-command)
;; 	("g" . 'push-mark-command)
;; 	("s" . 'delete-char)
;; 	("w" . 'cycle-spacing)
;; 	("z" . 'comment-dwim)
;; 	("3" . delete-window)
;; 	("4" . split-window-right)
;; 	("5" . balance-windows))
;; )

;; MODALKA QWERTY
(use-package modalka
  :bind
  (("<escape>" . 'modalka-on)
  ("C-x C-u" . 'upcase-dwim)
  :map modalka-mode-map
  ("+" . 'text-scale-increase)
  ("-" . 'text-scale-decrease)
  ("i" . 'ntdef/previous-line-and-deactivate)
  ("I" . 'previous-line)
  ("k" . 'ntdef/next-line-and-deactivate)
  ("K" . 'next-line)
  ("j" . 'backward-char)
  ("l" . 'forward-char)
  ("u" . 'ntdef/backward-word-and-mark)
  ("U" . 'backward-word)
  ("o" . 'ntdef/forward-word-and-mark)
  ("O" . 'forward-word)
  ("f" . 'modalka-off)
  (";" . 'ntdef/end-of-line-and-mark)
  (":" . 'end-of-line)
  ("h" . 'ntdef/beginning-of-line-and-mark)
  ("H" . 'beginning-of-line)
  ("m" . 'ntdef/end-of-paragraph-and-mark)
  ("," . 'ntdef/beginning-of-paragraph-and-mark)
  ("y" . 'undo)
  ("a" . 'execute-extended-command)
  ("t" . 'push-mark-command)
  ("d" . 'delete-char)
  ("w" . 'cycle-spacing)
  ("z" . 'comment-dwim)
  ("3" . 'delete-window)
  ("4" . 'split-window-right)
  ("5" . 'balance-windows)))

;; WHICH-KEY
(use-package which-key
  :init (which-key-mode))

;; COMPLETION
(use-package lsp-mode
  :ensure t
  :config

  ;; make sure we have lsp-imenu everywhere we have LSP
  ;; (require 'lsp-imenu)
  ;; (add-hook 'lsp-after-open-hook 'lsp-enable-imenu)

  ;; get lsp-python-enable defined
  ;; NB: use either projectile-project-root or ffip-get-project-root-directory
  ;;     or any other function that can be used to find the root directory of a project
  ;; (lsp-define-stdio-client lsp-python "python"
  ;;                          #'projectile-project-root
  ;;                          '("pyls"))

  ;; make sure this is activated when python-mode is activated
  ;; lsp-python-enable is created by macro above
  ;; (add-hook 'python-mode-hook
  ;;           (lambda ()
  ;;             (lsp-python-enable)))
  )

(use-package lsp-python-ms
  :ensure t
  :init (setq lsp-python-ms-auto-install-server t)
  :hook (python-mode . (lambda ()
                         (require 'lsp-python-ms)
                         (lsp))))  ; or lsp-deferred

(use-package lsp-java
  :hook (java-mode . #'lsp))

(use-package company
  :config
  (add-hook 'after-init-hook 'global-company-mode))

;; KUBERNETES
(use-package kubernetes
  :config
  (setq kubernetes-poll-frequency 3600
	kubernetes-redraw-frequency 3600))

(use-package emacs
  :config
  (add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p)

  ;; TODO
  (defun ntdef/display-completion-list (completions &optional common-substring))

  (defun ntdef/complete-at-point (start end collection &optional predicate)
    "FIXME"
    (unless (minibufferp)
      (with-output-to-temp-buffer "*Completions*"
	;; TODO figure out how to customize how display-completion-list looks
	(let* ((initial (buffer-substring-no-properties start end))
	       (limit (car (completion-boundaries initial collection predicate "")))
	       (all (all-completions initial collection predicate)))
	  (display-completion-list all)))))

  ;; (setq completion-in-region-function #'ntdef/complete-at-point)

  ;;   (add-hook 'post-self-insert-hook #'completion-at-point t)



  (setq display-buffer-alist
	'((".*\\*Completions.*"
           (display-buffer-at-bottom)
           (window-height . 0.16)
           (side . bottom)
           (slot . 0)
           (window-parameters . ((no-other-window . t)))))))



;; this  is  a
;; that I'm working on
;; right now
;; it's helpful to write these things up
;; that will help me remembed them later
(put 'narrow-to-region 'disabled nil)
